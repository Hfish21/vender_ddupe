from mongoengine import *
from datetime import datetime

connect(host='mongodb://localhost:27107/run1')

class CompanyLocation(EmbeddedDocument):
    created_at = DateTimeField(default=datetime.utcnow)
    country = StringField(default=None)
    state = StringField(default=None)
    address = StringField(required=True)
    coordinates = GeoPointField(default=None)

class CompanyOfficer(Document):
    created_at = DateTimeField(default=datetime.utcnow)
    agent_name = StringField(default=None)
    position = StringField(defaul=None)
    data_source = StringField(required=True, default=None) 
    meta_data = DictField(required=True, default={})

class CompanyIdentifier(Document):
    created_at = DateTimeField(default=datetime.utcnow)
    id_type = StringField(required=True)
    value = StringField(required=True)
    data_source = StringField(required=True, default=None) 
    meta_data = DictField(required=True, default={})

class Company(Document):
    created_at = DateTimeField(default=datetime.utcnow)
    vendor = ReferenceField(default=None)
    status = StringField(default=None, choices=['Active', 'Inactive'])
    branch = BooleanField(required=True, default=False)
    branches = ListField(ReferenceField('Company'), default=[])
    incorporation_date = DateTimeField(default=None)
    company_type = StringField(default=None)
    jurisdiction = StringField(default=None)
    location = EmbeddedDocumentField('CompanyLocation', default=None)
    officers = ListField(ReferenceField('CompanyOfficerInfo'), default=[])
    identifiers = ListField(ReferenceField('CompanyIdentifier'), default=[])
    data_source = StringField(required=True, default=None) 
    meta_data = DictField(default={})

class DomainContact(EmbeddedDocument):
    created_at = DateTimeField(default=datetime.utcnow)
    domain = ReferenceField('Domain', required=True)
    contact_type = StringField(required=True)
    name = StringField(required=True)
    company = ReferenceField('Company', default=False)
    street = StringField(default=None)
    city = StringField(default=None)
    state = StringField(default=None)
    postal_code = IntField(default=None)
    country = StringField(default=None)
    phone = StringField(default=None)
    phone_extension = StringField(default=None)
    fax = StringField(default=None)
    fax_extension = StringField(default=None)
    email = StringField(default=None)

class Domain(Document):
    created_at = DateTimeField(default=datetime.utcnow)
    link = URLField(required=True)
    website = ReferenceField('Website', default=None)
    registrar = StringField(default=None)
    registrar_abuse_email = StringField(default=None)
    registrar_abuse_phone = StringField(default=None)
    registered_on = DateTimeField(default=None)
    expires_on = DateTimeField(default=None)
    updated_on = DateTimeField(default=None)
    status = StringField(default=None)
    name_servers = ListField(StringField(), default=[])
    contacts = EmbeddedDocumentListField('DomainContact')
    data_source = StringField(required=True, default=None) 
    meta_data = DictField(default={})

class SocialMediaContact(EmbeddedDocument):
    created_at = DateTimeField(default=datetime.utcnow)
    media_type = StringField(required=True, choices=['Twitter', 'Facebook', 'LinkeIn', 'Youtube'])
    link = URLField(required=True)
    
class Website(Document):
    created_at = DateTimeField(default=datetime.utcnow)
    domain = ReferenceField('Domain')
    retrieveable = BooleanField(required=True)
    redirects = ListField(StringField(), required=True, default=[])
    redirects_unique = ListField(StringField(), required=True, default=[])
    redirect_end = StringField(required=True)
    redirect_count = IntField(required=True, default=0)
    copyright_info = StringField(default=None)
    phone_number = StringField(default=None)
    address = StringField(default=None)
    image_links = ListField(StringField(), default=[])
    image_logo = StringField(default=None)
    social_contacts = ListField(ReferenceField('SocialMediaContact'), default=[])
    meta_data = DictField(default={})

class Vendor(Document):
    created_at = DateTimeField(default=datetime.utcnow)
    name = StringField(required=True)
    notes = StringField(default=None)
    companies = ListField(ReferenceField('Company'), default=[])
    primary_company = ReferenceField('Company', default=None)
    domain = ReferenceField('Domain', default=None)
    meta_data = DictField(default={})
    
    




    ''' 
Vendor:
- Owner
- place of registration
- domain registration
- type of company


- what the company does


- date of creation 
- geolocation 
- redirect path
- contact info 
- social media acct
- possible alias's
- possible affiliates
'''